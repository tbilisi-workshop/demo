
provider "aws" {
  version = "~> 2.52" 
  region = "eu-west-2"
}

provider "tls" {
  version = "~> 2.1"
}

resource "tls_private_key" "ssh_key" {
  algorithm  = "RSA"
}

resource "aws_key_pair" "tbilisi_key" {
  key_name   = "tbilisi_key"
  public_key = tls_private_key.ssh_key.public_key_openssh
}

variable "server_count" {
  default = 2
}

data "aws_ami" "my_image" {
  most_recent = true
  filter {
    name   = "name"
    values = ["devops-ubuntu-18-04-x64*"]
  }
  owners = ["self"]
}

data "aws_subnet" "public_subnet" {
  cidr_block = "10.1.100.0/24"
}

data "aws_security_group" "security" {
  name = "workshop_security"
}

resource "aws_instance" "student_server" {
  count         = var.server_count
  instance_type = "t3.small"
  root_block_device {
    volume_size = "20"
  }
  tags = {
    Name = "tf_student_server_${format("%02d", count.index + 1)}"
  }
  key_name               = aws_key_pair.tbilisi_key.key_name

  ami                    = data.aws_ami.my_image.id
  subnet_id              = data.aws_subnet.public_subnet.id
  vpc_security_group_ids = [ data.aws_security_group.security.id ]
  
  connection {
    host        = coalesce(self.public_ip, self.private_ip)
    type        = "ssh"
    user        = "ubuntu"
    private_key = tls_private_key.ssh_key.private_key_pem
  }

}

output "servers_ips" {
  value = aws_instance.student_server[*].public_ip
}

resource "local_file" "private_key" {
  filename = "server.key"
  content  = tls_private_key.ssh_key.private_key_pem
}







