
terraform {
  required_version = ">= 0.12"
}

terraform {
  backend "s3" {
    bucket = "xa-london-2019"
    key    = "solution"
    region = "eu-west-1"
  }
}

provider "aws" {
  region = "eu-west-1"
}

provider "template" {
}

provider "local" {
}

provider "null" {
}

provider "random" {
}

