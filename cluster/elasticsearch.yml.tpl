
cluster.name: ${cluster_name}

network.host: 0.0.0.0

node.name: node-${node_index}
cluster.initial_master_nodes: ["node-1"]

discovery.seed_hosts: 
%{ for node in nodes ~}
- "${node}"
%{ endfor ~}