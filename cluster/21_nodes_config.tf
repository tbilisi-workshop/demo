
resource "local_file" "es_config" {
  count    = var.node_count
  content  = templatefile("elasticsearch.yml.tpl", {
    cluster_name = var.project_name,
    nodes = aws_instance.cluster_node.*.private_dns,
    node_index = count.index + 1
  })
  filename = "elasticsearch-${count.index}.yml"
}


resource "null_resource" "copy_cluster_config" {
  count = var.node_count
  triggers = {
    file_id = local_file.es_config[count.index].id
  }
  depends_on = [ 
    aws_instance.cluster_node, 
    local_file.es_config 
  ]
  connection {
    host        = aws_instance.cluster_node[count.index].public_ip
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("student.key")
  }
  provisioner "file" {
    source      = "elasticsearch-${count.index}.yml"
    destination = "/tmp/elasticsearch.yml"
  }
  provisioner "remote-exec" {
    inline = [
      "sudo cp /tmp/elasticsearch.yml /etc/elasticsearch",
      "sudo chown -R elasticsearch:elasticsearch /usr/share/elasticsearch",
      "sudo service elasticsearch restart",
    ]
  }
}
